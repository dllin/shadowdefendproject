import bagel.Input;
import bagel.util.Point;

import java.util.List;

/**
 * A regular slicer.
 */
public class RegularSlicer extends Slicer {

    /**
     * Creates a new Slicer
     *
     * @param polyline The polyline that the slicer must traverse (must have at least 1 point)
     */
    public RegularSlicer(List<Point> polyline, int targetPointIndex, Point point, String imageSrc) {
        super(polyline, targetPointIndex, point, imageSrc);

        this.speed = 2;
        this.health = 1;
        this.reward = 2;
        this.penalty = 1;
        this.spawnOnDeath = 0;
    }

    /**
     * Updates the current state of the slicer. The slicer moves towards its next target point in
     * the polyline at its specified movement rate.
     */
    @Override
    public void update(Input input) {

        super.update(input);
    }


}
