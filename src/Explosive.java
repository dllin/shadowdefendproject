import bagel.Input;
import bagel.util.Point;
/**
 * An explosive.
 */
public class Explosive extends Sprite{
    private static final double COOLDOWN = 2000;
    private double radius,frameCount;
    private boolean finished;
    private Point point;

    /**
     * Creates a new Explosive
     *
     * @param point The point where the image is to be drawn
     */
    public Explosive(Point point, String imageSrc){
        super(point,imageSrc);
        this.radius = 200;

        this.point = point;

        this.finished = false;
        this.frameCount = 0;

    }
    public Point getPoint(){
        return this.point;
    }
    /**
     * Updates the current state of the explosive. The explosive waits the allotted time and explodes.
     */
    @Override
    public void update(Input input) {
        frameCount += ShadowDefend.getTimescale();
        if (finished) {
            return;
        }
        if (frameCount*1000 / ShadowDefend.FPS >= COOLDOWN) {
            // Explodes after waiting COOLDOWN time
            finished = true;
        }
        super.update(input);
    }
    public boolean isFinished() {
        return finished;
    }


}
