import bagel.Input;
import bagel.util.Point;
import bagel.util.Vector2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * An airplane.
 */
public class Airplane extends Sprite{
    private static final String EXPLOSIVE_FILE = "res/images/explosive.png";
    private static final int HEIGHT = 768;
    private static final int WIDTH = 1024;
    private static final double TIME_MIN = 1;
    private static final double TIME_MAX = 2;
    private final List<Explosive> explosives;

    private double speed, frameCount, nothingTime;
    private int damage;
    private boolean finished, isVertical;
    private Random r = new Random();
    private Point explode;
    /**
     * Creates a new Airplane
     *
     * @param point The point where the image is to be drawn
     * @param isVertical Whether Airplane will go horizontally or vertically
     */
    public Airplane(Point point, boolean isVertical, String imageSrc){
        super(point,imageSrc);
        this.speed = 3;
        this.damage = 500;
        this.finished = false;
        this.isVertical = isVertical;
        this.frameCount = Integer.MAX_VALUE;
        this.nothingTime = Math.round((TIME_MIN + (TIME_MAX - TIME_MIN) * r.nextDouble())*1000);
        this.explosives = new ArrayList<>();
        this.explode = null;

    }
    public Point getExplode(){
        return this.explode;
    }
    public int getDamage(){
        return this.damage;

    }
    /**
     * Updates the current state of the airplane. The airplane moves towards its next target point
     * at its specified movement rate.
     */
    @Override
    public void update(Input input) {
        explode = null;
        frameCount += ShadowDefend.getTimescale();
        if (finished) {
            return;
        }
        // Let plane move either horizontally or vertically
        Point currentPoint = getCenter();
        Point targetPoint = new Point(currentPoint.x + 3,currentPoint.y);
        setAngle(Math.PI/2);

        if(isVertical){
            targetPoint = new Point(currentPoint.x,currentPoint.y + 3);
            setAngle(Math.PI);
        }

        Vector2 target = targetPoint.asVector();
        Vector2 current = currentPoint.asVector();
        Vector2 distance = target.sub(current);

        if (frameCount*1000 / ShadowDefend.FPS >= nothingTime &&(currentPoint.x < WIDTH && currentPoint.y < HEIGHT)) {
            // Add explosives at current point every 1-2 seconds
            explosives.add(new Explosive(currentPoint,EXPLOSIVE_FILE));
            nothingTime = Math.round((TIME_MIN + (TIME_MAX - TIME_MIN) * r.nextDouble())*1000);
            frameCount = 0;
        }

        // Check if we have reached the end
        if ((currentPoint.x >= WIDTH || currentPoint.y >= HEIGHT) && explosives.isEmpty()) {
            finished = true;
            return;
        }

        for (int i = explosives.size() - 1; i >= 0; i--) {
            if(!explosives.isEmpty()){

                Explosive v = explosives.get(i);
                v.update(input);

                if (v.isFinished()) {
                    // Set explosion to occur
                    explode = v.getPoint();
                    explosives.remove(i);
                }
            }

        }

        // Move towards the target point
        // We do this by getting a unit vector in the direction of our target, and multiplying it
        // by the speed of the slicer (accounting for the timescale)
        super.move(distance.normalised().mul(speed * ShadowDefend.getTimescale()));

        super.update(input);
    }
    public boolean isFinished() {
        return finished;
    }


}
