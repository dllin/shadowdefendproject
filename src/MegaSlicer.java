import bagel.Input;
import bagel.util.Point;

import java.util.List;

/**
 * A mega slicer.
 */
public class MegaSlicer extends Slicer {



    /**
     * Creates a new Slicer
     *
     * @param polyline The polyline that the slicer must traverse (must have at least 1 point)
     */
    public MegaSlicer(List<Point> polyline, int targetPointIndex, Point point,String imageSrc) {
        super(polyline, targetPointIndex, point,imageSrc);

        this.speed = 1.5;
        this.health = 2;
        this.reward = 10;
        this.penalty = 4;
        this.spawnOnDeath = 2;
    }

    /**
     * Updates the current state of the slicer. The slicer moves towards its next target point in
     * the polyline at its specified movement rate.
     */
    @Override
    public void update(Input input) {

        super.update(input);
    }


}
