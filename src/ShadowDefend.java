import bagel.*;
import bagel.Font;
import bagel.Image;

import bagel.map.TiledMap;
import bagel.util.Colour;
import bagel.util.Point;
import bagel.util.Rectangle;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


/**
 * ShadowDefend, a tower defence game.
 * Used Rohyl's template from Project 1
 */
public class ShadowDefend extends AbstractGame {
    // Resource files and constants
    private static final String RSLICER = "res/images/slicer.png";
    private static final String SSLICER = "res/images/superslicer.png";
    private static final String MSLICER = "res/images/megaslicer.png";
    private static final String ASLICER = "res/images/apexslicer.png";
    private static final int LIVES = 25;
    private static final int GOLD = 500;
    private static final int HEIGHT = 768;
    private static final int WIDTH = 1024;
    private static final int HALF_WIDTH = WIDTH/2;
    private static final int QUAR_WIDTH = WIDTH/4;
    private static final int EXP_RADIUS = 200;
    private static final String SHOP_FILE = "res/images/buypanel.png";
    private static final String STATUS_FILE = "res/images/statuspanel.png";
    private static final String TANK_FILE = "res/images/tank.png";
    private static final String TANK_PROJ_FILE = "res/images/tank_projectile.png";
    private static final String STANK_PROJ_FILE = "res/images/supertank_projectile.png";
    private static final String STANK_FILE = "res/images/supertank.png";
    private static final String PLANE_FILE = "res/images/airsupport.png";
    private static final String FONT_FILE = "res/fonts/DejaVuSans-Bold.ttf";
    private static final String INFO = "Key binds:\n\nS - Start Wave\nL - Increase Timescale\nK - Decrease Timescale";

    private static final String WAVES_FILE = "res/levels/waves.txt";
    // For coloured text
    private DrawOptions colour = new DrawOptions();

    // Change to suit system specifications. This could be
    // dynamically determined but that is out of scope.
    public static final double FPS = 60;

    // Initial timescale
    private static final int INTIAL_TIMESCALE = 1;

    // Timescale is made static because it is a universal property of the game and the specification
    // says everything in the game is affected by this
    private static int timescale = INTIAL_TIMESCALE;

    // Images for the buy and status panel
    private final Image shop, statusPanel, tankIcon, stankIcon, planeIcon;

    // Font for the buy and status panel
    private Font goldDisplay, keyInstruction, costText, statusText;

    // Image when placing tower
    private Image placingIcon;
    private String placedImage;

    // Entity lists
    private final List<Slicer> slicers;
    private final List<Projectile> projectiles;
    private final List<Tank> tanks;
    private final List<Airplane> planes;

    // Represents window
    private final Rectangle window;

    // Map file which changes according to level
    private String map_file = "res/levels/1.tmx";

    // The game map
    private TiledMap map;

    // The line slicers follow
    private List<Point> polyline;

    // Event data from waves.txt
    private List<String> event;
    private List<List<String>> events;
    private int spawnDelay;
    private int spawnCount;
    private int eventType;
    private String enemyType;

    // Frame counts
    private double frameCount;
    private double projCount;

    // Time for Delay event
    private int nothingTime;

    // Event variables
    private boolean waveStarted;
    private int level;
    private int spawnedSlicers;
    private int waveCounter;
    private int eventCounter;
    private int prevWaveState;
    private boolean eventWait;
    private boolean eventStarted = true;

    // For projectile to assign damage
    private int slicerIndex;

    // Whether a tank is placed or not
    private boolean tankBlocked;

    // Has new map been set
    private boolean mapSet;

    // Plane flight direction
    private boolean isVertical = false;

    // Whether a tank has a target
    private boolean hasTarget = false;

    // Ongoing key game variables
    private static int gold, lives, waveNumber, waveState;


    /**
     * Creates a new instance of the ShadowDefend game
     */
    public ShadowDefend() {

        super(WIDTH, HEIGHT, "ShadowDefend");
        // Initialise variables
        gold = GOLD;
        lives = LIVES;
        waveNumber = 1;
        waveState = 0;
        this.window = new Rectangle(0,0,WIDTH,HEIGHT);
        this.map = new TiledMap(map_file);
        this.shop = new Image(SHOP_FILE);
        this.statusPanel = new Image(STATUS_FILE);
        this.tankIcon = new Image(TANK_FILE);
        this.stankIcon = new Image(STANK_FILE);
        this.planeIcon = new Image(PLANE_FILE);
        this.goldDisplay = new Font(FONT_FILE,50);
        this.keyInstruction = new Font(FONT_FILE,16);
        this.costText = new Font(FONT_FILE,20);
        this.statusText = new Font(FONT_FILE,16);
        this.polyline = map.getAllPolylines().get(0);
        this.slicers = new ArrayList<>();
        this.projectiles = new ArrayList<>();
        this.tanks = new ArrayList<>();
        this.planes = new ArrayList<>();
        this.events = new ArrayList<>();
        this.event = new ArrayList<>();
        this.level = 1;
        this.spawnedSlicers = 0;
        this.waveStarted = false;
        this.frameCount = Integer.MAX_VALUE;
        this.projCount = Integer.MAX_VALUE;
        this.waveCounter = 1;
        this.eventCounter = 0;
        this.eventWait = false;
        this.mapSet = false;
        this.tankBlocked = false;

        try {
            File wavesText = new File(WAVES_FILE);
            Scanner fr = new Scanner(wavesText);
            while (fr.hasNextLine()) {
                events.add(Arrays.asList(fr.nextLine().split(",")));
            }
            fr.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * The entry-point for the game
     *
     * @param args Optional command-line arguments
     */
    public static void main(String[] args) {
        new ShadowDefend().run();
    }

    public static int getTimescale() {
        return timescale;
    }

    /**
     * Increases the timescale
     */
    private void increaseTimescale() {
        if(timescale!=5){
            timescale++;
        }

    }

    /**
     * Decreases the timescale but doesn't go below the base timescale
     */
    private void decreaseTimescale() {
        if (timescale > INTIAL_TIMESCALE) {
            timescale--;
        }
    }
    /**
     * Resets the game for new level but maintaining gold
     */
    private void resetLevel(){
        map = new TiledMap(map_file);
        mapSet = false;
        lives = LIVES;
        gold = GOLD;
        level += 1;
        slicers.clear();
        tanks.clear();
        planes.clear();
        projectiles.clear();
        polyline = map.getAllPolylines().get(0);
        eventCounter = 0;
        waveNumber = 1;
        waveCounter = 1;
        waveState = 0;
        waveStarted = false;
    }

    /**
     * Draws the shop panel
     */
    private void drawShopPanel(){
        //Draw shop panel at top left
        shop.drawFromTopLeft(0,0);

        // Draw icons
        tankIcon.draw(64,(shop.getHeight()/2)-10);
        stankIcon.draw(184,(shop.getHeight()/2)-10);
        planeIcon.draw(304,(shop.getHeight()/2)-10);

        // Draw information
        keyInstruction.drawString(INFO,(shop.getWidth()/2)-keyInstruction.getWidth(INFO)/2,20);

        // Draw gold amount
        if(gold >= 1000){
            // Add comma if gold count over 4 digits, can use for loop for more than 7 digits
            // not realistic to have over a million gold in this game
            String extra = Integer.toString(gold).substring(Integer.toString(gold).length() -3);
            String thousandth = Integer.toString(gold).substring(0,Integer.toString(gold).length() -3);
            goldDisplay.drawString("$"+ thousandth + "," + extra,shop.getWidth()-200,65);

        } else {
            goldDisplay.drawString("$"+ gold,shop.getWidth()-200,65);
        }

        // Draw the cost text for Tank with colouring
        // RED: gold not enough
        if(gold < 250){
            costText.drawString("$"+250,64-(tankIcon.getWidth()/2),
                    (tankIcon.getHeight()/2)+(shop.getHeight()/2+5),
                    colour.setBlendColour(Colour.RED));

        } else {
            costText.drawString("$"+250,64-(tankIcon.getWidth()/2),
                    (tankIcon.getHeight()/2)+(shop.getHeight()/2+5),
                    colour.setBlendColour(Colour.GREEN));
        }

        // Draw the cost text for SuperTank with colouring
        // RED: gold not enough
        if(gold < 600){
            costText.drawString("$"+600,184-(stankIcon.getWidth()/2),
                    (stankIcon.getHeight()/2)+(shop.getHeight()/2+5),
                    colour.setBlendColour(Colour.RED));

        } else {
            costText.drawString("$"+600,184-(stankIcon.getWidth()/2),
                    (stankIcon.getHeight()/2)+(shop.getHeight()/2+5),
                    colour.setBlendColour(Colour.GREEN));

        }

        // Draw the cost text for Plane with colouring
        // RED: gold not enough
        if(gold < 500){
            costText.drawString("$"+500,304-(planeIcon.getWidth()/2),
                    (planeIcon.getHeight()/2)+(shop.getHeight()/2+5),
                    colour.setBlendColour(Colour.RED));

        } else {
            costText.drawString("$"+500,304-(planeIcon.getWidth()/2),
                    (planeIcon.getHeight()/2)+(shop.getHeight()/2+5),
                    colour.setBlendColour(Colour.GREEN));

        }
    }

    /**
     * Draws the status panel
     */
    private void drawStatusPanel(){
        //Draw status panel at bottom
        statusPanel.drawFromTopLeft(0,HEIGHT - statusPanel.getHeight());

        // Draw wave number on left side
        statusText.drawString("Wave: " + waveNumber,5,HEIGHT-6);

        // Draw timescale with colouring
        // GREEN: over 1x timescale
        if(timescale>1){
            statusText.drawString("Timescale: " + timescale + ".0",
                    QUAR_WIDTH,
                    HEIGHT-6,
                    colour.setBlendColour(Colour.GREEN));

        } else {
            statusText.drawString("Timescale: " + timescale + ".0",QUAR_WIDTH,HEIGHT-6);
        }

        // Draw wave state in middle of panel
        switch(waveState) {
            case 0:
                statusText.drawString("Status: Awaiting Start",
                        HALF_WIDTH,
                        HEIGHT-6);
                break;
            case 1:
                statusText.drawString("Status: Wave In Progress",
                        HALF_WIDTH,
                        HEIGHT-6);
                break;
            case 2:
                statusText.drawString("Status: Placing",
                        HALF_WIDTH,
                        HEIGHT-6);
                break;
            case 3:
                statusText.drawString("Status: Winner!",
                        HALF_WIDTH,
                        HEIGHT-6);
                break;
        }

        // Draw number of remaining lives on right side
        statusText.drawString("Lives: " + lives,
                WIDTH-statusText.getWidth("Lives: " + lives)-5,
                HEIGHT-6);

    }
    /**
     * Get whether mouse pointer is hovering over an already placed tank
     *
     * @param input The current mouse/keyboard state
     */
    private void getTankBlocked(Input input){
        tankBlocked = false;
        // Loop through tanks
        for (int j = tanks.size() - 1; j >= 0; j--) {
            if (!tanks.isEmpty()) {
                Tank v = tanks.get(j);
                // Checks the mouse pointer does not intersect with a tank
                if(v.getRect().intersects(input.getMousePosition())){
                    tankBlocked = true;
                }
            }
        }
    }
    /**
     * Breakdown an event into usable data
     */
    private void getEventData(){
        if(event.size() == 3){
            // Delay event
            eventType = 0;
            waveNumber = Integer.parseInt(event.get(0));
            nothingTime = Integer.parseInt(event.get(2));
        } else {
            // Spawn event
            eventType = 1;
            waveNumber = Integer.parseInt(event.get(0));
            spawnCount = Integer.parseInt(event.get(2));
            enemyType = event.get(3);
            spawnDelay = Integer.parseInt(event.get(4));
        }

    }




    /**
     * Update the state of the game, potentially reading from input
     *
     * @param input The current mouse/keyboard state
     */
    @Override
    protected void update(Input input) {
        // Increase the frame counter by the current timescale
        frameCount += getTimescale();
        projCount += getTimescale();

        // Draw map from the top left of the window
        map.draw(0, 0, 0, 0, WIDTH, HEIGHT);


        //----------------------- Handle key presses ------------------------------------------------------------
        //                                      MOUSE KEY PRESSES
        //_______________________________________________________________________________________________________
        // User has left clicked
        if(input.wasPressed(MouseButtons.LEFT)){
            // Checks if mouse above the tank icon when left clicked and if enough gold
            if(tankIcon.getBoundingBoxAt(new Point(64,(shop.getHeight()/2)-10)).intersects(input.getMousePosition())
                    && gold >= 250){
                // Create image of tank and set wave state to Placing
                placingIcon = new Image(TANK_FILE);
                placedImage = "tank";
                if(waveState != 2){
                    prevWaveState = waveState;
                    waveState = 2;
                }
            }

            // Checks if mouse above the super tank icon when left clicked and if enough gold
            if(stankIcon.getBoundingBoxAt(new Point(184,(shop.getHeight()/2)-10)).intersects(input.getMousePosition())
                    && gold >= 600){
                // Create image of super tank and set wave state to Placing
                placingIcon = new Image(STANK_FILE);
                placedImage = "stank";
                if(waveState != 2){
                    prevWaveState = waveState;
                    waveState = 2;
                }
            }

            // Checks if mouse above the plane icon when left clicked and if enough gold
            if(planeIcon.getBoundingBoxAt(new Point(304,(shop.getHeight()/2)-10)).intersects(input.getMousePosition())
                    && gold >= 500){
                // Create image of plane and set wave state to Placing
                placingIcon = new Image(PLANE_FILE);
                placedImage = "plane";
                if(waveState != 2){
                    prevWaveState = waveState;
                    waveState = 2;
                }

            }
        }

        // User has selected an icon
        if(input.isUp(MouseButtons.LEFT) && placingIcon != null){
            // Check pointer still within window
            if(window.intersects(input.getMousePosition())){
                // Get whether pointer hovering over already placed tanks
                getTankBlocked(input);
                // Filter blocked areas
                if(!map.hasProperty((int)Math.round(input.getMouseX()),(int)Math.round(input.getMouseY()),"blocked")
                        && !shop.getBoundingBox().intersects(input.getMousePosition())
                        && !statusPanel.getBoundingBoxAt(new Point(HALF_WIDTH,HEIGHT - statusPanel.getHeight()/2)).intersects(input.getMousePosition())
                        && !tankBlocked){
                    // Draw icon
                    placingIcon.draw(input.getMouseX(),input.getMouseY());
                }
            }

        }
        // User has not placed tower by right clicking again
        if(input.wasPressed(MouseButtons.RIGHT) && placingIcon != null){

            // Reset wave state and placing image to previous wave state and null
            waveState = prevWaveState;
            placingIcon = null;
        }
        // User has placed tower by left clicking again, filter blocked areas
        if(input.wasPressed(MouseButtons.LEFT) && placingIcon != null
                && !map.hasProperty((int)Math.round(input.getMouseX()),(int)Math.round(input.getMouseY()),"blocked")
                && !shop.getBoundingBox().intersects(input.getMousePosition())
                && !statusPanel.getBoundingBoxAt(new Point(HALF_WIDTH,HEIGHT - statusPanel.getHeight()/2)).intersects(input.getMousePosition())
                && !tankBlocked){
            // Add tank tower if tank and update gold
            if(placedImage.equals("tank")){
                tanks.add(new Tank(input.getMousePosition(),TANK_FILE));
                gold-=250;
            }
            // Add super tank tower if super tank and update gold
            if(placedImage.equals("stank")) {
                tanks.add(new SuperTank(input.getMousePosition(), STANK_FILE));
                gold-=600;
            }
            // Add plane if plane and update gold
            if(placedImage.equals("plane")) {
                // Create new plane object with direction given
                if (isVertical) {
                    planes.add(new Airplane(new Point(input.getMouseX(), 0), isVertical, PLANE_FILE));
                    isVertical = false;
                } else {
                    planes.add(new Airplane(new Point(0, input.getMouseY()), isVertical, PLANE_FILE));
                    isVertical = true;
                }

                gold -= 500;

            }
            // Reset wave state and placing image to previous wave state and null after placing is complete
            waveState = prevWaveState;
            placingIcon = null;
        }
        //                                      KEYBOARD KEY PRESSES
        //_______________________________________________________________________________________________________
        // User has pressed S
        if (input.wasPressed(Keys.S)) {
            // Start a wave if wave state is not Wave in progress
            if(waveState != 1){
                waveStarted = true;
                // Set wave state to Wave in progress
                if(waveState<=1){
                    waveState = 1;
                }

            }

        }

        // User has pressed L
        if (input.wasPressed(Keys.L)) {
            increaseTimescale();
        }

        // User has pressed K
        if (input.wasPressed(Keys.K)) {
            decreaseTimescale();
        }

        // MAIN UPDATE ----------------------------------------------------------------------
        // Checks if user has lives and not beaten the levels yet
        if(waveState!=3 && lives>0){
            // Checks if a wave has started and is in progress
            if(waveStarted){
                // Run through each event
                if(eventCounter<events.size()){
                    event = events.get(eventCounter);
                    // Checks if event is part of a new wave and if event has completed
                    if(waveCounter == Integer.parseInt(event.get(0)) && !eventWait){
                        getEventData();
                        eventStarted = true;
                        eventCounter++;
                    // Since it is a new wave, increment wave counter and wait for user to begin next wave
                    } else if(waveCounter != Integer.parseInt(event.get(0)) && !eventWait){
                        waveCounter++;
                        waveStarted = false;
                    }
                // Events have reached the last event
                } else {
                    // Checks no slicers on map
                    if(slicers.isEmpty()){
                        // Set incremented level as new map, if not already set
                        if(!mapSet){
                            mapSet = true;
                            map_file = map_file.split(String.valueOf(level))[0]+(level+1)
                                    +map_file.split(String.valueOf(level))[1];
                        }
                        // If level does not exist, user has beaten all available levels
                        if(!(new File(map_file).exists())){
                            // Wave state set to Winner!
                            waveState = 3;
                        // Set up new level and reset
                        } else {
                            resetLevel();
                        }

                    }



                }
            // Wave has just finished so set to Awaiting and update gold
            } else {
                if(waveState == 1 && slicers.isEmpty()){
                    gold += waveNumber*100+150;
                    waveState = 0;
                }
            }

            // Run an event
            if(eventStarted){
                // Event is a spawn event
                if (eventType == 1) {
                    // Event not yet complete
                    eventWait = true;

                    // Spawn slicers with specified spawn count and delay
                    if (frameCount*1000 / FPS >= spawnDelay && spawnedSlicers != spawnCount) {
                        switch(enemyType) {
                            case "slicer":
                                slicers.add(new RegularSlicer(polyline,1,polyline.get(0),RSLICER));
                                break;
                            case "superslicer":
                                slicers.add(new SuperSlicer(polyline,1,polyline.get(0),SSLICER));
                                break;
                            case "megaslicer":
                                slicers.add(new MegaSlicer(polyline,1,polyline.get(0),MSLICER));
                                break;
                            case "apexslicer":
                                slicers.add(new ApexSlicer(polyline,1,polyline.get(0),ASLICER));
                                break;
                        }
                        // Increment spawned slicer count
                        spawnedSlicers += 1;
                        // Reset frame counter
                        frameCount = 0;

                    // Event has spawned specified amount
                    } else if (spawnedSlicers == spawnCount) {
                        // Reset wait, started and spawned slicer count for next event
                        eventWait = false;
                        eventStarted = false;
                        spawnedSlicers = 0;

                    }
                // Event is a delay event
                } else if (eventType == 0) {
                    eventWait = true;
                    // Wait out specified delay
                    if (frameCount*1000 / FPS >= nothingTime){
                        // Reset wait, started and frame count for next event
                        eventWait = false;
                        eventStarted = false;
                        // Reset frame counter
                        frameCount = 0;
                    }
                }
            }
            // Update all sprites, and remove them if they've finished

            // Update all tanks
            for (int j = tanks.size() - 1; j >= 0; j--) {
                if (!tanks.isEmpty()) {
                    // Select a tank object
                    Tank v = tanks.get(j);
                    // Set tank target to null
                    v.setTarget(null);
                    // Find closest slicer
                    for (int i = 0; i <= slicers.size() - 1; i++) {
                        if(!slicers.isEmpty()){
                            Slicer s = slicers.get(i);
                            // Checks if slicer within range and has health
                            if (v.getCenter().distanceTo(s.getCenter()) < v.getRadius() && s.getHealth() > 0 && !hasTarget) {
                                // Successfully found closest slicer and within tank range
                                v.setTarget(s);
                                slicerIndex = i;
                                hasTarget = true;
                            }
                        }
                    }
                    // Checks if tank has target
                    if(hasTarget){
                        // Shoot projectile at target once in every cooldown period
                        if (projCount*1000 / FPS >= v.getCooldown()) {
                            if(v.getDamage() == 1){
                                // Tank projectile
                                projectiles.add(new Projectile(v.getTankPoint(),v.getTarget(),v.getDamage(),slicerIndex,TANK_PROJ_FILE));
                            } else {
                                // Super Tank projectile
                                projectiles.add(new Projectile(v.getTankPoint(),v.getTarget(),v.getDamage(),slicerIndex,STANK_PROJ_FILE));
                            }
                            // Reset projectile frame count
                            projCount = 0;
                        }

                    }

                    v.update(input);
                    hasTarget = false;


                }
            }
            // Update all projectiles
            for (int i = projectiles.size() - 1; i >= 0; i--) {
                if(!projectiles.isEmpty()){
                    Projectile t = projectiles.get(i);

                    t.update(input);
                    // Projectile has hit a slicer
                    if (t.isFinished()) {
                        if(t.getSlicerIndex()<slicers.size()){
                            // Remove appropriate health from slicer
                            slicers.get(t.getSlicerIndex()).setHealth(slicers.get(t.getSlicerIndex()).getHealth()-t.getDamage());
                        }

                        // Remove projectile
                        projectiles.remove(i);
                    }

                }

            }
            // Update all planes
            for (int i = planes.size() - 1; i >= 0; i--) {
                if(!planes.isEmpty()){
                    Airplane v = planes.get(i);
                    v.update(input);
                    // Check if any of the plane's explosives are going to explode
                    if(v.getExplode()!=null){
                        // One of the planes explosives is ready to explode
                        // Create a range for explosion to occur
                        Rectangle r = new Rectangle(v.getExplode().x-EXP_RADIUS,
                                v.getExplode().y-EXP_RADIUS,
                                EXP_RADIUS*2,
                                EXP_RADIUS*2);
                        // Remove appropriate health from all slicers in range
                        for (int j = slicers.size() - 1; j >= 0; j--) {
                            if(slicers.get(j).getRect().intersects(r)){
                                slicers.get(j).setHealth(slicers.get(j).getHealth()-v.getDamage());
                            }
                        }
                    }
                    // Remove plane
                    if (v.isFinished()) {
                        planes.remove(i);
                    }
                }

            }
            // Update all slicers
            for (int i = slicers.size() - 1; i >= 0; i--) {
                if(!slicers.isEmpty()){
                    Slicer s = slicers.get(i);

                    s.update(input);
                    // Checks if specified slicer is finished
                    if (s.isFinished()) {
                        // Checks if slicer was finished by depleting health
                        if(s.getHealth()<=0){
                            // Spawn slicers on death if so
                            if(s.getPenalty() == 2){
                                for (int j = 0; j < s.getSpawnOnDeath(); j++) {
                                    slicers.add(new RegularSlicer(polyline, s.getTargetPointIndex(), s.getCurrentPoint(), RSLICER));
                                }

                            } else if(s.getPenalty() == 4){
                                for (int j = 0; j < s.getSpawnOnDeath(); j++) {
                                    slicers.add(new SuperSlicer(polyline,s.getTargetPointIndex(),s.getCurrentPoint(),SSLICER));
                                }

                            } else if(s.getPenalty() == 16){
                                for (int j = 0; j < s.getSpawnOnDeath(); j++) {
                                    slicers.add(new MegaSlicer(polyline,s.getTargetPointIndex(),s.getCurrentPoint(),MSLICER));
                                }

                            }
                            // Update gold for slicer kill
                            gold += s.getReward();
                        } else {
                            // Update lives if slicer completed the run
                            lives -= s.getPenalty();
                        }
                        // Remove slicer
                        slicers.remove(i);
                    }

                }

            }
        } else if(lives<=0){
            lives = 0;
        }







        //Draw shop panel
        drawShopPanel();

        // Draw status panel
        drawStatusPanel();






    }
}
