import bagel.Input;
import bagel.util.Point;

import java.util.List;

/**
 * A super slicer.
 */
public class SuperSlicer extends Slicer {



    /**
     * Creates a new Slicer
     *
     * @param polyline The polyline that the slicer must traverse (must have at least 1 point)
     */
    public SuperSlicer(List<Point> polyline, int targetPointIndex, Point point,String imageSrc) {
        super(polyline,targetPointIndex,point, imageSrc);

        this.speed = 1.5;
        this.health = 1;
        this.reward = 15;
        this.penalty = 2;
        this.spawnOnDeath = 2;

    }

    /**
     * Updates the current state of the slicer. The slicer moves towards its next target point in
     * the polyline at its specified movement rate.
     */
    @Override
    public void update(Input input) {

        super.update(input);
    }


}
