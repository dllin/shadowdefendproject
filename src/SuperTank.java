import bagel.Input;
import bagel.util.Point;
/**
 * A super tank.
 */
public class SuperTank extends Tank{


    /**
     * Creates a new Tank
     *
     * @param point The point where the image is to be drawn
     */
    public SuperTank(Point point, String imageSrc){
        super(point,imageSrc);
        this.radius = 150;
        this.damage = 3;
        this.cooldown = 500;

    }
    /**
     * Updates the current state of the tank. The tank faces its next target.
     */
    @Override
    public void update(Input input) {

        super.update(input);
    }



}
