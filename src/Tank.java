import bagel.Input;
import bagel.util.Point;

/**
 * A tank.
 */
public class Tank extends Sprite{
    protected double radius,cooldown;
    protected int damage;
    protected Point tankPoint;
    protected Slicer target;
    /**
     * Creates a new Tank
     *
     * @param point The point where the image is to be drawn
     */
    public Tank(Point point, String imageSrc) {
        super(point, imageSrc);
        this.radius = 100;
        this.damage = 1;
        this.cooldown = 1000;
        this.tankPoint = point;
        this.target = null;

    }
    public void setTarget(Slicer givenTarget){
        this.target = givenTarget;
    }
    public Point getTankPoint(){
        return this.tankPoint;
    }
    public Slicer getTarget(){
        return this.target;
    }
    public int getDamage(){
        return this.damage;
    }
    public double getRadius(){
        return this.radius;
    }
    public double getCooldown(){
        return this.cooldown;
    }
    /**
     * Updates the current state of the tank. The tank faces its next target.
     */
    @Override
    public void update(Input input) {
        if(target != null){
            // Obtain where we currently are, and where we want to be
            Point currentPoint = getCenter();
            Point targetPoint = target.getCenter();
            setAngle(Math.atan2(targetPoint.y - currentPoint.y, targetPoint.x - currentPoint.x)+Math.PI/2);
        }

        super.update(input);
    }



}
