import bagel.Input;
import bagel.util.Point;
import bagel.util.Vector2;

/**
 * A projectile.
 */
public class Projectile extends Sprite {
    private boolean finished;
    private double speed;
    private int damage,slicerIndex;
    private Slicer target;

    /**
     * Creates a new Projectile
     *
     * @param point The point where the image is to be drawn
     * @param damage Projectile's damage
     * @param target Projectile's target slicer
     * @param slicerIndex Index of target slicer in slicers
     */
    public Projectile(Point point, Slicer target,int damage,int slicerIndex, String imageSrc){
        super(point, imageSrc);
        this.target = target;
        this.speed = 10;
        this.damage = damage;
        this.slicerIndex = slicerIndex;
    }
    public int getSlicerIndex(){
        return this.slicerIndex;
    }

    public int getDamage() {
        return this.damage;
    }
    /**
     * Updates the current state of the projectile. The projectile moves towards its next target point
     * at its specified movement rate.
     */
    @Override
    public void update(Input input) {
        if (finished) {
            return;
        }

        // Obtain where we currently are, and where we want to be
        Point currentPoint = getCenter();
        Point targetPoint = target.getCenter();
        // Convert them to vectors to perform some very basic vector math
        Vector2 targetVector = targetPoint.asVector();
        Vector2 currentVector = currentPoint.asVector();
        Vector2 distance = targetVector.sub(currentVector);

        // Check if we have reached the end
        if (getRect().intersects(target.getRect())) {
            finished = true;
            return;
        }

        // Move towards the target point
        // We do this by getting a unit vector in the direction of our target, and multiplying it
        // by the speed of the slicer (accounting for the timescale)
        super.move(distance.normalised().mul(speed * ShadowDefend.getTimescale()));
        // Update current rotation angle to face target point
        setAngle(Math.atan2(targetPoint.y - currentPoint.y, targetPoint.x - currentPoint.x));
        super.update(input);
    }
    public boolean isFinished() {
        return finished;
    }
}
