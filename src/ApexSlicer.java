import bagel.Input;
import bagel.util.Point;

import java.util.List;

/**
 * An apex slicer.
 */
public class ApexSlicer extends Slicer {



    /**
     * Creates a new Slicer
     *
     * @param polyline The polyline that the slicer must traverse (must have at least 1 point)
     */
    public ApexSlicer(List<Point> polyline,int targetPointIndex, Point point,String imageSrc) {
        super(polyline, targetPointIndex, point, imageSrc);

        this.speed = 0.75;
        this.health = 25;
        this.reward = 150;
        this.penalty = 16;
        this.spawnOnDeath = 4;
    }

    /**
     * Updates the current state of the slicer. The slicer moves towards its next target point in
     * the polyline at its specified movement rate.
     */
    @Override
    public void update(Input input) {

        super.update(input);
    }


}
